# Ejercicio 1: Programa del cálculo del salario
# para darle al empleado 1.5 veces la tarifa horaria para todas
# las horas trabajadas que excedan de 40.

#__autora__  = "Martha Cango"
#__email__   = "martha.cango@unl.edu.ec"

horas=int(input("Ingrese las horas:"))
tarifa=float(input("Ingrese la tarifa por hora:"))

if horas>40:
    resto=horas-40
    extra=(resto*1.5)*tarifa
    salario = (40*tarifa)+ extra
    print("El salario es:", salario)
    print("Gracias por consultar aquí!")
else:
    salario=horas*tarifa
    print("El salario es:", salario)
    print("Gracias por su consulta aquí")

