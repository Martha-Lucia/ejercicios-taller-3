#Ejercicio 2: Programa del salario usando try y except, de modo
# que el programa sea capaz de gestionar entradas no numericas
# con elegancia, mostrando un mensaje y saliendo del programa.

#__autora__  = "Martha Cango"
#__email__   = "martha.cango@unl.edu.ec"

try:
    horas = int(input("Ingrese las horas:"))
    tarifa = float(input("Ingrese la tarifa por hora:"))

    if horas > 40:
        resto = horas - 40
        extra = (resto * 1.5) * tarifa
        salario = (40 * tarifa) + extra
        print("El salario es:", salario)
        print("Gracias por consultar aquí!")
    else:
        salario = horas * tarifa
        print("El salario es:", salario)
        print("Gracias por consultar aquí")
except:
    print("Error, por favor ingrese un número")
