# Ejercicio 3: Programa que solicite una puntuaciòn entre 0.0 y 1.0.
# Si la puntuaciòn esta fuera de ese rango, muestra un mensaje de
# error. Si la puntuaciòn esta entre 0.0 y 1.0 muestra la
# calificaciòn usando la tabla siguiente:

#__autora__  = "Martha Cango"
#__email__   = "martha.cango@unl.edu.ec"

try:
    puntuacion= float(input("Introduza puntuaciòn:"))
    #validacion del rango  de la puntuacion
    if puntuacion >=0 and puntuacion <=1.0:
        if puntuacion >= 0.9:
            print("Sobresaliente")
        elif puntuacion >=0.8:
            print("Notable")
        elif puntuacion >=0.7:
            print("Bien")
        elif puntuacion >=0.6:
            print("Suficiente")
        elif puntuacion <0.6:
            print("insuficiente")
    else:
        print("Puntuaciòn incorrecta")
except:
    print("Puntuaciòn incorrecta")


